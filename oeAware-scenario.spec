Name:       oeAware-scenario
Version:    v1.0.2
Release:    2
Summary:    %{name} provides low-overhead scenario awareness 
License:    MulanPSL2
URL:        https://gitee.com/openeuler/%{name}
Source0:    %{name}-%{version}.tar.gz
Patch1:     0001-fix-config-path.patch
BuildRequires: cmake make gcc-c++

%description
%{name} provides low-overhead scenario awareness

%prep
%autosetup -n %{name}-%{version} -p1

%build
cd thread_aware
mkdir build 
cd build 
cmake .. -DCMAKE_BUILD_TYPE=Debug
make %{?_smp_mflags}

%install
mkdir -p %{buildroot}%{_libdir}/oeAware-plugin/
install -D -m 0740 ./thread_aware/build/*.so %{buildroot}%{_libdir}/oeAware-plugin/
install -D -m 0640 ./thread_aware/thread_scenario.conf %{buildroot}%{_libdir}/oeAware-plugin/

%files
%attr(0640, root, root) %{_libdir}/oeAware-plugin/thread_scenario.conf
%attr(0440, root, root) %{_libdir}/oeAware-plugin/libthread_scenario.so

%changelog
* Mon June 3 2024 fly_1997 <flylove7@outlook.com> - v1.0.2-2
- fix config path

* Fri May 31 2024 zhoukaiqi <zhoukaiqi@huawei.com> - v1.0.2-1
- refactor interface

* Wed May 15 2024 fly_1997 <flylove7@outlook.com> -v1.0.1-2
- fix warning message

* Sat May 11 2024 fly_1997 <flylove7@outlook.com> -v1.0.1-1
- update version to v1.0.1

* Thu Apr 18 2024 fly_1997 <flylove7@outlook.com> -v1.0.0-1
- Package init
